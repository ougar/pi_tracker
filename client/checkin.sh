#!/bin/bash
#
# Checkin to my py_tracker
#
# Gather some simple information about the status of the raspberry by
# and submit it to my pi_tracker with a simple curl command

# Read secret token
. $(dirname $0)/token.sh
# Url to tracker website
url="http://ougar.dk/pi_tracker/checkin.php"

if [[ "$*" =~ "-v" ]]; then verbose=1; else verbose=0; fi
if [[ "$*" =~ "-d" ]]; then dryrun=1; else dryrun=0; fi

# Start gather information about host
########################################
hostname=$(hostname)
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Hostname:" "$hostname"

# NOTE: YOU CAN HAVE MORE THAN ONE!!!!!!
intip=$(hostname -I | awk '{print $1}')
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Internal IP:" "$intip"

# Get uptime and how much of that has been idle time from proc/uptime
uptime=$(printf "%.*f\n" 0 $(cat /proc/uptime | cut -d' ' -f1))
idle=$(printf "%.*f\n" 0 $(cat /proc/uptime | cut -d' ' -f2))
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Uptime:" "$uptime"
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Idle:" "$idle"

# Are we on cable og wireless
cable=$(cat /sys/class/net/eth0/carrier)
if [ "$cable" == "1" ]; then 
  nettype="cable"; 
fi
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Network type:" "$nettype"

# If we are on wireless, what is the SSID of the wlan
ssid=""
[ $verbose -eq 1 ] && printf "%-14s %s\n" "SSID:" "$ssid"

# Possibility to add extra information
extra=""
[ $verbose -eq 1 ] && printf "%-14s %s\n" "Extra:" "$extra"

# Assemble GET variables
args="token=$token&hostname=$hostname&ip=$intip&uptime=$uptime&idle=$idle&nettype=$nettype&ssid=$ssid&extra=$extra"

# Print curl command
if [ $verbose -eq 1 ]||[ $dryrun -eq 1 ]; then
  echo curl "$url?$args"
fi

# If -d (dryrun) was found, then don't actually perform the curl command
if [ $dryrun -eq 1 ]; then exit; fi

# Make curl-call to pi_tracker website
response=$(curl -s "$url?$args")

# Check if command was successfull
if [ "$respone" != "OK" ]; then exit 0;
else
  echo "Error checking in. Response was: $response"
  exit 1
fi
