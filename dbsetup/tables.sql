drop tables if exists pi_tracker;

create table pi_tracker (
  id int not null auto_increment primary key,
  stamp timestamp,
  hostname varchar(30),
  public_ip int unsigned,
  private_ip int unsigned,
  public_ipv varchar(15) generated always as (inet_ntoa(public_ip)),
  private_ipv varchar(15) generated always as (inet_ntoa(private_ip)),
  nettype enum("cable","wireless"),
  ssid varchar(30),
  uptime int,
  idle int,
  extra varchar(255),
  index hname (hostname));
