<?php

require("./token.php");
$token=$_GET["token"];

if (!$secrettoken) {
  http_response_code(403);
  die("Cannot find access token");
}

if ($token != $secrettoken) {
  http_response_code(403);
  die();
}

require("getvar.inc");
require("kdb.inc");

$dbh=MyDatabase::connect("diverse2");
$get=new GetVars();

$private_ip = $get->add("ip",null,0);
$hostname   = $get->add("hostname",null,"");
$uptime     = $get->add("uptime",null,0);
$idle       = $get->add("idle",null,0);
$nettype    = $get->add("nettype",null,0);
$ssid       = $get->add("ssid",null,"");
$extra      = $get->add("extra",null,"");

if (!preg_match("/[-a-zA-Z0-9]+/",$hostname)) die("Invalid hostname: $hostname\n");

if (!preg_match("/^[0-9.]*$/",$private_ip)) {echo("Invalid private ip: $private_ip\n"); $private_ip=0; }
if (!preg_match("/[0-9]*/",$uptime))        {echo("Invalid uptime: $uptime\n"); $uptime=0; } 
if (!preg_match("/[0-9]*/",$idle))          {echo("Invalid idle time: $idle\n"); $idle=0; } 
if ($nettype=="cable" || $nettype=="wireless") $nettype="'$nettype'"; else $nettype="NULL";
if ($nettype=="wireless") {
  $ssid="'".$dbh->real_escape_string($ssid)."'";
} else $ssid="NULL";
if ($extra) {
  $extra="'".$dbh->real_escape_string($extra)."'";
} else $extra="NULL";

$public_ip=$_SERVER["REMOTE_ADDR"];

$sql=sprintf("insert into pi_tracker (hostname,public_ip,private_ip,nettype,ssid,uptime,idle,extra) values ".
             "('%s',inet_aton('%s'),inet_aton('%s'),%s,%s,%d,%d,%s)",
             $hostname,$public_ip,$private_ip,$nettype,$ssid,$uptime,$idle,$extra);

$res=$dbh->kquery($sql);
echo "OK\n"

?>
